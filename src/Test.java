import jeu.Jeu;
import joueur.Joueur;
import personnage.Personnage;
import personnage.humain.Humain;
import personnage.tauren.Tauren;

public class Test {
    public static void main(String[] args) {
        /*
        Joueur joueur = new Joueur("toussaint");
        System.out.println(joueur);
        joueur.ajouterPersonnage(new Tauren("tor", 20, 8));
        System.out.println(joueur);
         */
        Joueur joueur1 = new Joueur("Paul");
        Tauren taurenJ1 = new Tauren("Hector",15,10);
        Humain humainJ1 = new Humain("Jean",15);
        joueur1.ajouterPersonnage(taurenJ1);
        joueur1.ajouterPersonnage(humainJ1);

        Joueur joueur2 = new Joueur("Lucien");
        Tauren taurenJ2 = new Tauren("Hercule",20,5);
        Humain humainJ2 = new Humain("Marie",10);
        joueur2.ajouterPersonnage(taurenJ2);
        joueur2.ajouterPersonnage(humainJ2);

        for (int i=0;i<400;i++){
            Jeu jeu = new Jeu("AtelierPOO",10,20);
            jeu.ajouterJoueur(joueur1);
            jeu.ajouterJoueur(joueur2);

            jeu.initialiserCases();

            jeu.lancerJeu();

            jeu.afficherCases();

            jeu.afficherParticipants();

            jeu.afficherResultats();
        }


    }
}
