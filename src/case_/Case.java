package case_;

import jeu.Jeu;
import obstacle.Obstacle;
import personnage.Personnage;
import personnage.humain.Humain;
import personnage.tauren.Tauren;

public class Case {

    private int gain;
    private Personnage perso = null;
    private Obstacle obs = null;

    public Case(Obstacle obs, int gain){
        this.obs = obs;
        this.gain = gain;
    }

    public Case(int gain){
        this.gain = gain;
    }

    public int getPenalite(){
        int result = 0;
        if (obs != null){
            result = obs.getPenalite();
        }
        return result;
    }

    public void placerPersonnage(Personnage perso){
        this.perso = perso;
    }

    public void placerObstacle(Obstacle obs){
        this.obs = obs;
    }

    public void enleverPersonnage(){
        perso = null;
    }

    public boolean estLibre(){
        return perso == null && obs == null;
    }

    public boolean sansObstacle(){
        return obs == null;
    }
    public boolean sansPerso(){
        return perso == null;
    }

    @Override
    public String toString(){
        String result = "";
        if (estLibre()){
            result = Jeu.ANSI_GREEN + "Libre (gain = " + gain + ")" + Jeu.ANSI_RESET;
        }else if (!sansObstacle()){
            result = Jeu.ANSI_RED + "Obstacle (penalité = -" + getPenalite() + ")" + Jeu.ANSI_RESET;
        }else if (!sansPerso()){
            if (perso instanceof Humain humain){
                result = humain + Jeu.ANSI_RED + " (penalité = -" + gain + ")" + Jeu.ANSI_RESET;
            }else if (perso instanceof Tauren tauren){
                result = tauren + Jeu.ANSI_RED +  " (penalité = -" + gain + ")" + Jeu.ANSI_RESET;
            }
        }
        return result;
    }

    public int getGain() {
        return gain;
    }

    public Personnage getPerso() {
        return perso;
    }

    public void resetCase(){
        enleverPersonnage();
        obs = null;
    }
}
