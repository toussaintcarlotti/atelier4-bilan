package jeu;

import case_.Case;
import joueur.Joueur;
import obstacle.Obstacle;
import personnage.Personnage;
import personnage.humain.Humain;
import personnage.tauren.Tauren;

import java.util.ArrayList;

public class Jeu {
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";


    private String titre;
    private static final int NBJOUEURMAX = 6;
    private final int NBCASES = 200;
    private final ArrayList<Joueur> listeJoueurs = new ArrayList<>();
    private final ArrayList<Case> cases = new ArrayList<>();
    private final int nbEtapes;
    private final int nbObstaclesMax;
    private static int scoreMax;

    public Jeu(String titre, int nbEtapes, int nbObstacles){
        this.titre = titre;
        this.nbEtapes = nbEtapes;
        nbObstaclesMax = nbObstacles;
    }
    
    public void ajouterJoueur(Joueur j){
        if (NBJOUEURMAX > listeJoueurs.size()){
            listeJoueurs.add(j);
        }

    }
    
    public ArrayList<Personnage> tousLesPersos(){
        ArrayList<Personnage> listResult = new ArrayList<>();
        for (Joueur joueur: listeJoueurs) {
            listResult.addAll(joueur.getListPersos());
        }
        return listResult;
    }

    public void initialiserCases(){
        int nbObstacles = 0;
        int penalite;
        int gain;
        for (int i=0;i<NBCASES;i++) {
            gain = 1 + (int)(Math.random() * ((NBCASES - 1) + 1));
            if (gain % 5 == 0){
                if (nbObstaclesMax > nbObstacles){
                    penalite = gain * 2;
                    cases.add(new Case(new Obstacle(penalite),gain));
                    nbObstacles += 1;
                }else {
                    cases.add(new Case(gain));
                }
            }else {
                cases.add(new Case(gain));
            }
        }
    }


    public void lancerJeu(){
        boolean test;
        int i;

        for (Personnage perso: tousLesPersos()) {
            test = false;
            i = 0;
            while (i < cases.size() && !test){
                if (cases.get(i).estLibre()){
                    cases.get(i).placerPersonnage(perso);
                    perso.deplacer(i,cases.get(i).getGain());
                    test = true;
                }
                i += 1;
            }
        }
        int posSouhait;
        for (i=0;i<nbEtapes;i++){
            for (Personnage perso: tousLesPersos()) {
                posSouhait = perso.positionSouhaitee();
                while (posSouhait > NBCASES - 1){
                    cases.get(perso.getPosition()).resetCase();
                    perso.resetPosition();
                    posSouhait = perso.positionSouhaitee();
                }

                if (cases.get(posSouhait).estLibre()){
                    cases.get(perso.getPosition()).resetCase();
                    perso.deplacer(posSouhait, cases.get(posSouhait).getGain());
                    cases.get(posSouhait).placerPersonnage(perso);
                }else if (!cases.get(posSouhait).sansObstacle()){
                    perso.penaliser(cases.get(posSouhait).getPenalite());
                }else if (!cases.get(posSouhait).sansPerso()){
                    perso.penaliser(cases.get(posSouhait).getGain());
                }

            }
        }
    }

    public void afficherCases(){
        for (int i=0;i<cases.size();i++){
            System.out.println("Case " + i + ": " + cases.get(i));
        }
    }

    public void afficherParticipants(){

        System.out.println("LISTE DES JOUEURS");
        for (Joueur joueur:listeJoueurs) {
            System.out.println("-------------------------");
            System.out.println(joueur);
        }
    }
    public void afficherResultats(){
        int maxPoints = -1;
        Joueur gagnant = null;
        for (Joueur joueur:listeJoueurs){
            if (joueur.getNbPoints() > maxPoints){
                maxPoints = joueur.getNbPoints();
                gagnant = joueur;
            }
        }
        if (gagnant != null){
            String result = "***********************************************\n" +
                    "Le gagnant est " + gagnant.getNom() + " avec " + gagnant.getNbPoints() + " points\nRecord";
            if (gagnant.getNbPoints() > scoreMax){
                result +=  " battu" ;
            }else{
                result += " non battu" ;
            }
            result += ": Ancien score maximum afficher "+ scoreMax;
            System.out.println(result);
            if (scoreMax < gagnant.getNbPoints()){
                scoreMax = gagnant.getNbPoints();

            }
        }

        for (Joueur joueur: listeJoueurs){
            joueur.resetNbPointJoueur();
        }
    }


}
