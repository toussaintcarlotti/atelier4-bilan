package joueur;

import personnage.Personnage;

import java.util.ArrayList;

public class Joueur {
    private final String nom;
    private final String code;
    private static int nbJoueurs = 0;
    private int nbPoints;
    private final ArrayList<Personnage> listPersos = new ArrayList<>();

    public Joueur(String nom){
        nbJoueurs += 1;
        this.nom = nom;
        code = "J" + nbJoueurs;
    }

    public void ajouterPersonnage(Personnage personnage){
        listPersos.add(personnage);
        personnage.setProprietaire(this);
    }

    public void modifierPoints(int points){
        nbPoints += points;
        if (nbPoints < 0 ){
            nbPoints = 0;
        }
    }

    public boolean peutJouer(){
        return listPersos.size() > 0;
    }
    @Override
    public String toString(){
        String result = code + " " + nom + "(" + nbPoints + ")" ;
        if (peutJouer()){
            result += " avec " + listPersos.size() + " personnages";
        }else{
            result += " aucun personnage";
        }
        return result;
    }

    public ArrayList<Personnage> getListPersos(){
        return listPersos;
    }

    public int getNbPoints() {
        return nbPoints;
    }

    public String getNom() {
        return nom;
    }

    public void resetNbPointJoueur(){
        nbPoints = 0;
    }
}
