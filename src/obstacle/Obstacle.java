package obstacle;

public class Obstacle {
    private final int penalite;

    /**
     * constructeur obstacle
     * @param penalite valeur de la pénalité
     */
    public Obstacle(int penalite){
        this.penalite = penalite;
    }

    /**
     * accesseur pénalité
     * @return retourne la valeur de la pénalité
     */
    public int getPenalite() {
        return penalite;
    }
}
