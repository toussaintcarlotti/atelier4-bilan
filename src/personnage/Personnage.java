package personnage;

import joueur.Joueur;

public abstract class Personnage {
    protected int position;
    private Joueur proprietaire;
    private final String nom;
    private final int age;

    public Personnage(String nom, int age){
        this.nom = nom;
        this.age = age;
    }

    public void setProprietaire(Joueur proprietaire){
        this.proprietaire = proprietaire;
    }

    public void deplacer(int destination, int gain){
        position = destination;
        proprietaire.modifierPoints(gain);
    }

    public void penaliser(int penalite){
        proprietaire.modifierPoints(- penalite);
    }

    @Override
    public String toString() {
        return  nom;
    }

    public abstract int positionSouhaitee();

    public int getPosition() {
        return position;
    }

    public abstract void resetPosition();
}
