package personnage.humain;

import jeu.Jeu;
import personnage.Personnage;

public class Humain extends Personnage {

    private int nbDeplacements;
    private int niveau;

    public Humain(String nom, int age){
        super(nom, age);
        nbDeplacements = 0;
        niveau = 1;
    }

    @Override
    public int positionSouhaitee(){
        return this.position + ( niveau * nbDeplacements);
    }

    @Override
    public void deplacer(int destination, int gain){
       super.deplacer(destination, gain);
       nbDeplacements += 1;
       if (nbDeplacements == 4){
           niveau = 2;
       }
       if (nbDeplacements == 6){
           niveau = 3;
       }
    }

    @Override
    public String toString(){
        return Jeu.ANSI_YELLOW + "Humain  " + super.toString() + Jeu.ANSI_RESET;
    }


    public void resetPosition() {
        this.position = 0;
        nbDeplacements = 0;

    }
}
