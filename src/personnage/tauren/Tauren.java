package personnage.tauren;

import jeu.Jeu;
import personnage.Personnage;

public class Tauren extends Personnage {
    private int taille;

    public Tauren(String nom, int age, int taille){
        super(nom, age);
        this.taille = taille;
    }

    @Override
    public int positionSouhaitee(){
        return this.position + (1 + (int)(Math.random() * ((taille - 1) + 1)));
    }

    @Override
    public String toString(){
        return Jeu.ANSI_PURPLE + "Tauren " + super.toString() + Jeu.ANSI_RESET;
    }

    public void resetPosition() {
        this.position = 0;


    }
}
